<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', TRUE);
error_reporting(E_ALL);

session_start();

header("Content-type: text/html; charset=utf-8");

require_once(__DIR__. '/db.php');
//配列を文字列に変換する(DBに登録した配列を使う時は、逆にデコードをする)
$hobbyData = $_SESSION["hobby"];
$_SESSION["hobby"] = serialize($hobbyData);





try {

  //例外処理を投げる（スロー）ようにする
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $statement = $dbh->prepare("INSERT INTO users (name, email, password, tel, prefectures, hobby, gender, contact, creation_time) VALUES (:name, :email, :password, :tel, :prefectures, :hobby, :gender, :contact, :creation_time)");

  if ($statement) {
    // タイム・ゾーンの設定
    date_default_timezone_set('Asia/Tokyo');

    $date = new DateTime();
    $date = $date->format('Y-m-d H:i:s');

    // プレースホルダへ実際の値を設定する
    $statement->bindValue(':name', $_SESSION["name"], PDO::PARAM_STR);
    $statement->bindValue(':email', $_SESSION["email"], PDO::PARAM_STR);
    $statement->bindValue(':password', $_SESSION["password"], PDO::PARAM_STR);
    $statement->bindValue(':tel', $_SESSION["tel"], PDO::PARAM_STR);
    $statement->bindValue(':prefectures', $_SESSION["prefectures"], PDO::PARAM_STR);
    $statement->bindValue(':hobby', $_SESSION["hobby"], PDO::PARAM_STR);
    $statement->bindValue(':gender', $_SESSION["gender"], PDO::PARAM_STR);
    $statement->bindValue(':contact', $_SESSION["contact"], PDO::PARAM_STR);
    $statement->bindValue(':creation_time', $date, PDO::PARAM_STR);



    //　実行する
    if (!$statement->execute()) {
      $errors['error'] = "登録失敗しました。 ";
    }

    //データベース切断
    $dbh = null;


  }


} catch (PDOException $e) {
    print('Error:'.$e->getMessage());
		$errors['error'] = "データベース接続失敗しました。";
}



 ?>


 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>登録完了</title>
   </head>
   <body>

登録完了！



<a href="http://192.168.33.15"><button type="button" name="singlebutton" class="btn btn-primary" id="singlebutton">戻る</button></a>



   </body>
 </html>
