<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

session_start();

//　文字化けを治す
header("Content-type: text/html; charset=utf-8");

$errorMsg = "";


 ?>


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>新規登録フォーム</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>
<body>



<form class="form-horizontal" action="confirm.php" method="post">
    <fieldset>

    <!-- Form Name -->
    <legend style=background-color:#65ace4;><h1 style=text-align:center;>新規登録フォーム</h1></legend>


    <!-- パンくずリスト-->
    <ol class="breadcrumb">
        <li><a href="http://192.168.33.15"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>トップ</a></li>
        <li class="active">新規登録フォーム</li>
      </ol>


      <div class="container">

        <!-- 入力エラー出力 -->
        <div>

        <?php if (isset($_SESSION["errorMsg"])):  ?>
        <?php if ((count($_SESSION['errorMsg']) >= 1)): ?>
          <?php foreach ($_SESSION['errorMsg'] as $message): ?>
            <?php echo "<font color=RED>・$message</font>"."<br>" ?>
          <?php endforeach; ?>
        <?php endif; ?>
      <?php endif; ?>

        </div>

    <!-- Text input 名前-->
    <div class="form-group" style="margin-top:30px;">
      <label class="col-md-4 control-label" for="name">氏名 <span class="label label-danger">必須</span></label>
      <div class="col-md-4">
      <input name="name" class="form-control input-md" id="name" type="text" placeholder="例：鈴木太郎">

      </div>
    </div>




    <!-- Text input　メールアドレス-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">メールアドレス <span class="label label-danger">必須</span></label>
      <div class="col-md-4">
      <input name="email" class="form-control input-md" id="email" type="email" placeholder="例：suzuki.taro@gmail.com 半角">

      </div>
    </div>


     <!-- Text input　パスワード-->
     <div class="form-group">
        <label class="col-md-4 control-label" for="password">パスワード <span class="label label-danger">必須</span></label>
        <div class="col-md-4">
        <input name="password" class="form-control input-md" id="password" type="password" placeholder="パスワード 半角">

        </div>
      </div>

    <!-- Text input 電話番号　-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="tel">電話番号 <span class="label label-danger">必須</span></label>
      <div class="col-md-4">
      <input name="tel" class="form-control input-md" id="tel" type="tel" placeholder="09011112222 ハイフンなし 半角">

      </div>
    </div>

    <!-- Select Basic　都道府県 -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="prefectures">都道府県 <span class="label label-danger">必須</span></label>
      <div class="col-md-4">
        <select name="prefectures" class="form-control" id="prefectures">
          <option value="選択">選択</option>
          <option value="東京">東京</option>
          <option value="名古屋">名古屋</option>
          <option value="大阪">大阪</option>
          <option value="その他">その他</option>
        </select>
      </div>
    </div>


    <!-- インラインチェックボックス　趣味 -->
        <div class="form-group">
            <label class="control-label col-md-4" for="checkbox">趣味 <span class="label label-info">任意</span></label>
            <div class="col-md-4">
              <div class="checkbox">
                  <label class="checkbox-inline">
                      <input name="hobby[]" type="checkbox" value="野球"> 野球
                    </label>
                    <label class="checkbox-inline">
                      <input name="hobby[]" type="checkbox" value="サッカー"> サッカー
                    </label>
              </div>
            </div>
          </div>



           <!-- インラインラジオボタン　性別 -->
           <div class="form-group">
              <label class="control-label col-md-4" for="checkbox">性別 <span class="label label-info">任意</span></label>
              <div class="col-md-4">
              <div class="radio-inline" name="gender">
                  <input name="gender" type="radio" value="男性" name="gender" id="man">男性
                </div>
                <div class="radio-inline">
                  <input name="gender" type="radio" value="女性" name="gender" id="woman">女性
                </div>
              </div>
            </div>



    <!-- テキストエリア　お問い合わせ -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="contact">お問い合わせ <span class="label label-info">任意</span></label>
          <div class="col-md-4">
            <textarea name="contact" placeholder="お問い合わせ" rows="3" class="form-control" id="contact"></textarea>
          </div>
        </div>



    <!-- Button　送信ボタン -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="singlebutton"></label>
      <div class="col-sm-offset-5 col-sm-7 col-xs-offset-4 col-xs-8">
        <a href="http://192.168.33.15"><button type="button" name="singlebutton" class="btn btn-primary" id="singlebutton">戻る</button></a>
        <button name="singlebutton" class="btn btn-primary" id="singlebutton">確認する</button>
      </div>
    </div>

  </div>

    </fieldset>
    </form>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
    </html>
