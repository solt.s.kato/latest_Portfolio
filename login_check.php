<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

session_start();
header("Content-type: text/html; charset=utf-8");

//クロスサイトリクエストフォージェリ（CSRF）対策のトークン判定
// if ($_POST['token'] != $_SESSION['token']){
//	echo "不正アクセスの可能性あり";
//	exit();
// }

// クリックジャッキング対策
header("X-FRAME-OPTIONS: SAMEORIGIN");

// クロスサイトリクエストフォージュリ(CSRF)対策のトークン判定
if (isset($_POST["csrfToken"])) {
    if ($_POST["csrfToken"] !== $_SESSION["csrfToken"]) {
        echo "不正アクセス(CSRF)の可能性あり。正規の画面から来てください。";
        exit();
    }
}

// 変数の初期化
$email = '';

// セッション変数の定義
$_SESSION["email"] = $_POST["email"];
$_SESSION["password"] = $_POST["password"];

// エラーメッセージの配列の初期化
$errors = array();

// エラーメッセージの初期化
$_SESSION['errors'] = array();

// データベース接続
require_once(__DIR__. '/db.php');

// 前後にある半角全角スペースを削除する関数
function spaceTrim($str) {

    // 行頭
    $str = preg_replace('/^[   ]+/u', '', $str);
    // 末尾
    $str = preg_replace('/[   ]+$/u', '', $str);

    return $str;
}


 // ログインボタンが押された場合
 if (isset($_POST["login"])) {


    // POSTされたデータを各変数に入れる
     $email = filter_input(INPUT_POST, 'email');
     $password = filter_input(INPUT_POST, 'password');

     // 前後にある半角全角スペースを削除
     $email = spaceTrim($email);
     $password = spaceTrim($password);

     // アカウント入力判定
     if (empty($email)) {
         $errors["email"] = "Emailが入力されていません。";
     } elseif (!empty($email) && !preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
         $errors["email"] = "Emailを正しく入力してください。";
     }

     // パスワード入力判定
     if (empty($password)) {
         $errors["password"] = "パスワードが入力されていません。";
     } elseif (preg_match('/^[0-9a-zA-Z]{1,7}$/', $password)) {
         $errors["password"] = "パスワードは8文字以上で入力してください。";
     } else {
         $password_hide = str_repeat('*', strlen($password));
     }
 }

// エラーがなければ実行する
if (count($errors) == 0) {
    try {

        // 例外処理を投げる(スロー)ようにする
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // アカウントで検索
        $statement = $dbh->prepare("SELECT * FROM users WHERE email=(:email)");
        $statement->bindValue(":email", $email, PDO::PARAM_STR);
        $statement->execute();

        // アカウントが一致
        if ($row = $statement->fetch()) {
            $db_password = $row["password"];

            // パスワードが一致
            if ($password === $db_password) {

          // セッションハイジャック対策
                session_regenerate_id(true);

                $_SESSION["email"] = $email;
                header("Location: login_admin.php");
                exit();
            } else {
                $errors["email"] = "Email及びパスワードが一致しません。";
            }
        } else {
            $errors["password"] = "Email及びパスワードが一致しません。";
        }

        if (count($errors) > 0) {
            $_SESSION['errors'] = $errors;
            foreach ($errors as $value) {
                echo $value;
            }
            header("Location: login_form.php");
        }



        // データベース切断
        $dbh = null;
    } catch (PDOException $e) {
        print("Error:" .$e->getMessage());
        die();
    }
} else {
    $_SESSION['errors'] = $errors;
    foreach ($errors as $value) {
        echo $value;
    }
    header("Location: login_form.php");
}



?>


<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>ログイン確認画面</title>
  </head>
  <body>

    <h1>ログイン確認画面</h1>

  </body>
</html>
