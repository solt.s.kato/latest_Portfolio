<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

session_start();
header("Content-type: text/html; charset=utf-8");

// ログイン状態のチェック
if (!isset($_SESSION["email"])) {
  header("Location: login_form.php");
  exit();
}

// セッション変数を全て解除
$_SESSION = array();

// セッションクッキーを削除
if (isset($_COOKIE["PHPSESSID"])) {
  setcookie("PHPSESSID", '', time() - 1800, '/');
}

// セッションを破棄する
session_destroy();


 ?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>ログアウト</title>
  </head>
  <body>

    <p>ログアウトしました。</p>

    <a href="login_form.php">ログイン画面へ</a>

  </body>
</html>
