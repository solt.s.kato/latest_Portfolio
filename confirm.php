<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', TRUE);
error_reporting(E_ALL);

session_start();

//　文字化けを治す
header("Content-type: text/html; charset=utf-8");

// 変数の定義(初期化)
$name = "";
$email = "";
$password = "";
$tel = "";
$prefectures = "";
$hobby = "";
$gender = "";
$contact = "";

$date = date_default_timezone_set('Asia/Tokyo');


$form_Content = array($name, $email, $password, $tel, $prefectures, $hobby, $gender, $contact);

// エラーメッセージの配列の初期化
$errorMsg = array();
// セッション変数の初期化
$_SESSION['errorMsg'] = array();


// XSS対策 サニタイズ（無毒化）
function h($string) {

    if(is_array($string)){
        return array_map("h", $string);

    } else {

        return htmlspecialchars($string, ENT_QUOTES, "UTF-8");
    }
}

  // XSS対策 (hrefやsrcの値がURLか確認する)
  function urlCheck($form_Content) {
      if (!preg_match("/^(https?:.+)$/", $form_Content)) {
          return $errorMsg[] = "不正を検知しました。";
      } else {
          return true;
      }
  }



// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


    $_SESSION["name"] = filter_input(INPUT_POST, 'name');
    $_SESSION["email"] = filter_input(INPUT_POST, 'email');
    $_SESSION["password"] = filter_input(INPUT_POST, 'password');
    $_SESSION["tel"] = filter_input(INPUT_POST, 'tel');
    $_SESSION["prefectures"] = filter_input(INPUT_POST, 'prefectures');
    $_SESSION["hobby"] = filter_input(INPUT_POST, 'hobby', FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    $_SESSION["gender"] = filter_input(INPUT_POST, 'gender');
    $_SESSION["contact"] = filter_input(INPUT_POST, 'contact');

//
// 未入力チェック
//

// エラーメッセージの配列の初期化
$errorMsg = array();

// 名前の未入力チェック
if (empty($_SESSION["name"])) {
    $errorMsg[] = "名前を入力してください。";
}

// メールアドレスの未入力チェック
if (empty($_SESSION["email"])) {
    $errorMsg[] = "メールアドレスを入力してください。";
}

// パスワードの未入力チェック
if (empty($_SESSION["password"])) {
    $errorMsg[] = "パスワードを入力してください。";
}

// 電話番号の未入力チェック
if (empty($_SESSION["tel"])) {
    $errorMsg[] = "電話番号を入力してください。";
}

// 都道府県の未入力チェック
if ($_SESSION["prefectures"] == "選択") {
    $errorMsg[] = "都道府県を選択してください。";
}

  //
  // 文字数チェック
  //

  // 名前の文字数チェック
  if (strlen($_SESSION["name"]) >= 60) {
    $errorMsg[] = "氏名が長すぎます。";
  }

  // パスワード文字数チェック(8文字以上か)
  if (preg_match("/^[a-zA-Z1-9]{1,7}$/", $_SESSION["password"])) {
      $errorMsg[] = "パスワードは8文字以上で入力してください。";
  }

  //　電話番号の文字数チェック(10文字 or 11文字)
  if (strlen($_SESSION["tel"]) >= 1 && strlen($_SESSION["tel"]) <= 9 && preg_match("/^[0-9]+$/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号は10文字か11文字で入力してください。";
  } elseif (strlen($_SESSION["tel"]) >= 12 && preg_match("/^[0-9]+$/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号は10文字か11文字で入力してください。";
  }

  //
  // 形式チェック
  //

  // メールアドレス形式チェック
  if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_SESSION["email"]) && $_SESSION["email"] !== "") {
      $errorMsg[] = "メールアドレスに間違いがあります。";
  }

  // 電話番号の形式チェック
  if (preg_match("/[-]+/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号はハイフンなしで入力してください。";
  }

  //
  // 半角英数字チェック
  //

  //　電話番号の数字チェック
  if (!preg_match("/[0-9]/", $_SESSION["tel"]) && $_SESSION["tel"] !== "") {
    $errorMsg[] = "電話番号は半角数字で入力してください。";
  }


  // 電話番号の全角チェック Todo
  //if (strlen($tel) !== mb_strlen($tel, "UTF-8")) {
    //  $errorMsg[] = "電話番号に全角が含まれています。";
  //}


  if (count($errorMsg) >= 1) {
      $_SESSION['errorMsg'] = $errorMsg;
      foreach ($errorMsg as $error) {
          echo $error;
      }

      header("location: http://192.168.33.15/registration_form.php");
      exit;

  } else {
    echo "";
  }



}

 ?>

 <!DOCTYPE html>
 <html lang="ja">
 <head>
     <meta charset="utf-8">
     <title>確認フォーム</title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

 </head>
 <body>


   <form class="form-horizontal" action="registration_insert.php" method="post">
     <fieldset>

       <!-- Form Name -->
       <legend style=background-color:#65ace4;><h1 style=text-align:center;>確認フォーム</h1></legend>

       <ol class="breadcrumb">
           <li><a href="http://192.168.33.15"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>トップ</a></li>
           <li><a href="http://192.168.33.15/registration.html">新規登録フォーム</a></li>
           <li class="active">確認フォーム</li>
         </ol>

    <div class="container">

   	<div class="form-group">
   		<label class="col-sm-5 control-label">氏名</label>
   		<div class="col-sm-5">
   			<p class="form-control-static"><?php echo h($_SESSION["name"]); ?></p>
   		</div>
   	</div>

   	<div class="form-group">
   		<label class="col-sm-5 control-label">メールアドレス</label>
   		<div class="col-sm-5">
   			<p class="form-control-static"><?php echo h($_SESSION["email"]); ?></p>
   		</div>
   	</div>

   	<div class="form-group">
   		<label class="col-sm-5 control-label">パスワード</label>
   		<div class="col-sm-5">
   			<p class="form-control-static"><?php echo h($_SESSION["password"]); ?></p>
   		</div>
   	</div>


    <div class="form-group">
      <label class="col-sm-5 control-label">電話番号</label>
      <div class="col-sm-5">
        <p class="form-control-static"><?php echo h($_SESSION["tel"]); ?></p>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-5 control-label">都道府県</label>
      <div class="col-sm-5">
        <p class="form-control-static"><?php echo h($_SESSION["prefectures"]); ?></p>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-5 control-label">趣味</label>
      <div class="col-sm-5">
          <p class="form-control-static"><?php

          if (isset($_SESSION["hobby"])) {
              foreach ($_SESSION["hobby"] as $value) {
                  echo h($value);
              }
              echo " ";
          }


          ?></p>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-5 control-label">性別</label>
      <div class="col-sm-5">
          <p class="form-control-static"><?php echo h($_SESSION["gender"]); ?></p>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-5 control-label">お問い合わせ</label>
      <div class="col-sm-5">
        <p class="form-control-static"><?php echo h($_SESSION["contact"]); ?></p>
      </div>
    </div>


    <div class="button-group">
      <div class="col-md-5 col-sm-offset-5 col-sm-7 col-xs-offset-4 col-xs-8">
        <a href="http://192.168.33.15/registration_form.php"><button type="button" name="singlebutton" class="btn btn-primary" id="singlebutton">戻る</button></a>
        <button type="submit" class="btn btn-primary">送信する</button>
        <p class="help-block" style=margin-top:15px;>以上の内容でよろしければ、送信ボタンを押してください。</p>
      </div>
    </div>





</div>
      </fieldset>
   </form>



   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
   </body>
   </html>
