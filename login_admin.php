<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

session_start();
header("Content-type: text/html; charset=utf-8");



$email = $_SESSION["email"];

// ログイン状態のチェック
if (!isset($_SESSION["email"])) {
  header("Location: login_form.php");
  exit();
}

function h($string) {
  return htmlspecialchars($string, ENT_QUOTES, "UTF-8");
}

 ?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>ログイン</title>
  </head>
  <body>

    <?php echo h($email)."さん、こんにちは!"; ?>

    <a href="logout.php">ログアウトする</a>

  </body>
</html>
