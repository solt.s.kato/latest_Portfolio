<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

// 文字化け防止
header("Content-type: text/html; charset=UTF-8");

// エラーメッセージの初期化
$errors = array();
// 配列の初期化
$rows = array();

// XSS対策 サニタイズ（無毒化）
function h($string)
{
    return htmlspecialchars($string, ENT_QUOTES, "UTF-8");
}

require_once(__DIR__. "/db.php");


try {

    //例外処理を投げる（スロー）ようにする
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $dbh->prepare('SELECT * FROM country WHERE rank like :search or nation LIKE :search or food like :search or reason like :search');

    if ($statement) {

        $search = filter_input(INPUT_POST, "search");
        $like_search = "%$search%";

        // プレースホルダーへ実際の値を設定する
        $statement->bindValue(":search", $like_search, PDO::PARAM_STR);


        if ($statement->execute()) {
            // レコード件数取得
            $row_count = $statement->rowCount();

            while ($row = $statement->fetch()) {
                $rows[] = $row;
            }
        } else {
            $errors["error"] = "検索失敗しました。";
        }

        // データベース切断
      //  $dbh = null;
    }
} catch (PDOexception $e) {
    print('Error:'.$e->getMessage());
    $errors["error"] = "データベース接続失敗しました。";
}


?>


<!DOCTYPE html>
<html lang="ja">
<head>
		<meta charset="utf-8">
		<title>確認フォーム</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>
<body>


    <?php if (count($errors) == 0): ?>

    <?php echo h($search); ?>で検索しました。
    <p><?php echo h($row_count) ?>件です。</p>

    <table class="table table-hover">
      <thead>
      <tr>
          <th>順位</th>
          <th>国</th>
          <th>食べたい食べ物</th>
          <th>理由</th>
      </tr>
  </thead>
  <tbody>
                      <?php foreach ((array)$rows as $row) : ?>
                          <tr>
                              <th><?php echo h($row["rank"]) ?></th>
                              <td><?php echo h($row["nation"]) ?></td>
                              <td><?php echo h($row["food"]) ?></td>
                              <td><?php echo h($row["reason"]) ?></td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
 </table>

    <?php elseif (count($errors) > 0): ?>
      <?php foreach ((array)$errors as $value): ?>
          <p><?php  echo h($value); ?></p>
        <?php endforeach; ?>
    <?php endif; ?>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>

</html>
