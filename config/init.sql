create databese portfolio;

grant all on portfolio .* to andrew@localhost IDENTIFIED BY 'andrew135';

use portfolio

create table users (
  id int not null auto_increment primary key,
  name varchar(20),
  email varchar(255) unique,
  password varchar(255),
  tel int,
  prefectures varchar(255),
  hobby varchar(255),
  gender enum('男性', '女性'),
  contact varchar(255),
  creation_time  TIMESTAMP  not null DEFAULT null
);

create table country(
  id int(11) NOT NULL auto_increment PRIMARY KEY,
    rank varchar(255) NOT NULL,
    nation varchar(255) NOT NULL,
    food varchar(255) NULL,
    reason varchar(255) NULL
    );

desc users;
